﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OrderAdmin.aspx.cs" Inherits="OrderAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" DataKeyNames="orders_id" BackColor="#CCCCCC" 
    BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" 
    CellSpacing="2" ForeColor="Black">
        <RowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="orders_id" 
                HeaderText="订单编号" SortExpression="orders_id" ReadOnly="True" />
            <asp:BoundField DataField="orders_user_id" HeaderText="用户ID" 
                SortExpression="orders_user_id" />
            <asp:BoundField DataField="orders_time" HeaderText="订单时间" 
                SortExpression="orders_time" />
            <asp:BoundField DataField="orders_status" HeaderText="订单状态" 
                SortExpression="orders_status" />
            <asp:BoundField DataField="orders_amount" HeaderText="订单金额" 
                SortExpression="orders_amount" />
            <asp:BoundField DataField="orders_shipaddr" HeaderText="收货地址" 
                SortExpression="orders_shipaddr" />
            <asp:BoundField DataField="orders_tele" HeaderText="联系电话" 
                SortExpression="orders_tele" />
            <asp:BoundField DataField="orders_name" HeaderText="联系人" 
                SortExpression="orders_name" />
            <asp:CommandField ShowEditButton="True" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
    SelectCommand="SELECT * FROM [Orders_info]" 
        DeleteCommand="DELETE FROM [Orders_info] WHERE [orders_id] = @orders_id" 
        InsertCommand="INSERT INTO [Orders_info] ([orders_id], [orders_user_id], [orders_time], [orders_status], [orders_amount], [orders_shipaddr], [orders_tele], [orders_name]) VALUES (@orders_id, @orders_user_id, @orders_time, @orders_status, @orders_amount, @orders_shipaddr, @orders_tele, @orders_name)" 
        UpdateCommand="UPDATE [Orders_info] SET [orders_user_id] = @orders_user_id, [orders_time] = @orders_time, [orders_status] = @orders_status, [orders_amount] = @orders_amount, [orders_shipaddr] = @orders_shipaddr, [orders_tele] = @orders_tele, [orders_name] = @orders_name WHERE [orders_id] = @orders_id">
    <DeleteParameters>
        <asp:Parameter Name="orders_id" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="orders_user_id" Type="Int32" />
        <asp:Parameter Name="orders_time" Type="DateTime" />
        <asp:Parameter Name="orders_status" Type="Int32" />
        <asp:Parameter Name="orders_amount" Type="Double" />
        <asp:Parameter Name="orders_shipaddr" Type="String" />
        <asp:Parameter Name="orders_tele" Type="String" />
        <asp:Parameter Name="orders_name" Type="String" />
        <asp:Parameter Name="orders_id" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="orders_id" Type="Int32" />
        <asp:Parameter Name="orders_user_id" Type="Int32" />
        <asp:Parameter Name="orders_time" Type="DateTime" />
        <asp:Parameter Name="orders_status" Type="Int32" />
        <asp:Parameter Name="orders_amount" Type="Double" />
        <asp:Parameter Name="orders_shipaddr" Type="String" />
        <asp:Parameter Name="orders_tele" Type="String" />
        <asp:Parameter Name="orders_name" Type="String" />
    </InsertParameters>
</asp:SqlDataSource>
</asp:Content>
