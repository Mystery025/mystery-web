﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BookAdmin.aspx.cs" Inherits="BookShop.BookAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <asp:Button ID="Button1" runat="server" Text="搜索" />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" 
        OnSelectedIndexChanged="GridView1_SelectedIndexChanged" 
    DataKeyNames="book_id" BackColor="#CCCCCC" BorderColor="#999999" 
    BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" 
    ForeColor="Black">
        <RowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="book_id" HeaderText="ID" ReadOnly="True" 
                SortExpression="book_id" />
            <asp:BoundField DataField="isbn_id" HeaderText="ISBN" 
                SortExpression="isbn_id" />
            <asp:BoundField DataField="book_name" HeaderText="名称" 
                SortExpression="book_name" />
            <asp:BoundField DataField="book_class" HeaderText="类别" 
                SortExpression="book_class" />
            <asp:BoundField DataField="book_author" HeaderText="作者" 
                SortExpression="book_author" />
            <asp:BoundField DataField="book_publisher" HeaderText="出版社" 
                SortExpression="book_publisher" />
            <asp:BoundField DataField="book_price" HeaderText="价格" 
                SortExpression="book_price" />
            <asp:BoundField DataField="book_inventory" HeaderText="库存数量" 
                SortExpression="book_inventory" />
            <asp:BoundField DataField="book_image_id" HeaderText="图片ID" 
                SortExpression="book_image_id" />
            <asp:BoundField DataField="book_other" HeaderText="其他说明" 
                SortExpression="book_other" />
            <asp:CommandField ShowEditButton="True" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        SelectCommand="SELECT * FROM [Book_info]" 
        ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" 
        DeleteCommand="DELETE FROM [Book_info] WHERE [book_id] = @book_id" 
        InsertCommand="INSERT INTO [Book_info] ([book_id], [isbn_id], [book_name], [book_class], [book_author], [book_publisher], [book_price], [book_inventory], [book_image_id], [book_other]) VALUES (@book_id, @isbn_id, @book_name, @book_class, @book_author, @book_publisher, @book_price, @book_inventory, @book_image_id, @book_other)" 
        UpdateCommand="UPDATE [Book_info] SET [isbn_id] = @isbn_id, [book_name] = @book_name, [book_class] = @book_class, [book_author] = @book_author, [book_publisher] = @book_publisher, [book_price] = @book_price, [book_inventory] = @book_inventory, [book_image_id] = @book_image_id, [book_other] = @book_other WHERE [book_id] = @book_id">
        <DeleteParameters>
            <asp:Parameter Name="book_id" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="isbn_id" Type="String" />
            <asp:Parameter Name="book_name" Type="String" />
            <asp:Parameter Name="book_class" Type="String" />
            <asp:Parameter Name="book_author" Type="String" />
            <asp:Parameter Name="book_publisher" Type="String" />
            <asp:Parameter Name="book_price" Type="Double" />
            <asp:Parameter Name="book_inventory" Type="Int32" />
            <asp:Parameter Name="book_image_id" Type="Int32" />
            <asp:Parameter Name="book_other" Type="String" />
            <asp:Parameter Name="book_id" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="book_id" Type="Int32" />
            <asp:Parameter Name="isbn_id" Type="String" />
            <asp:Parameter Name="book_name" Type="String" />
            <asp:Parameter Name="book_class" Type="String" />
            <asp:Parameter Name="book_author" Type="String" />
            <asp:Parameter Name="book_publisher" Type="String" />
            <asp:Parameter Name="book_price" Type="Double" />
            <asp:Parameter Name="book_inventory" Type="Int32" />
            <asp:Parameter Name="book_image_id" Type="Int32" />
            <asp:Parameter Name="book_other" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>
