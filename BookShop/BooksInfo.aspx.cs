﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BooksInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        int selectedRow = ((GridView)sender).SelectedIndex;
        string book_id = GridView1.SelectedRow.Cells[0].Text;
        string urlAddress = "BooksDetails.aspx?book_id="+book_id;
        Response.Redirect(urlAddress);  
    }
}
