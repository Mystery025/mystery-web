﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
        
    }
    protected void Login1_LoggedIn(object sender, EventArgs e)
    {
        // 获取登录用户信息
        MembershipUser user = Membership.GetUser(Login1.UserName);
        if (user != null)
        {
            // 查询数据库，判断用户是否是管理员
            bool isAdmin = CheckIfUserIsAdmin(user.UserName);
            Session["IsAdmin"] = isAdmin;
        }
    }

    // 查询数据库，检查用户是否是管理员
    private bool CheckIfUserIsAdmin(string username)
    {
        bool isAdmin = false;

        string Connstr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(Connstr))
        {
            connection.Open();
            using (SqlCommand command = new SqlCommand("SELECT user_limit FROM User_info WHERE user_name = @Username", connection))
            {
                command.Parameters.AddWithValue("@Username", username);
                object result = command.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    // 如果user_limit为0，则用户是管理员
                    isAdmin = Convert.ToInt32(result) == 0;
                }
            }
            connection.Close();
        }
        return isAdmin;
    }
}
