﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // 获取登录用户信息
        MembershipUser user = Membership.GetUser(Page.User.Identity.Name);
        //HttpContext.Current.Response.Write("<script>alert('" + Page.User.Identity.Name + "')</script>");
        if (user != null)
        {
            // 查询数据库，判断用户是否是管理员
            bool isAdmin = CheckIfUserIsAdmin(user.UserName);
            Session["IsAdmin"] = isAdmin;
        }
        MenuItem adminMenu = Menu1.FindItem("管理员功能");
        if (adminMenu != null)
        {
            adminMenu.Enabled = false;
        }
        if (!IsPostBack)
        {
            // 检查是否是管理员，如果是管理员则显示管理员功能菜单
            if (Session["IsAdmin"] != null && (bool)Session["IsAdmin"])
            {
                adminMenu.Enabled = true;
            }
            else
            {
                // 如果不是管理员，可以隐藏管理员功能菜单
                adminMenu.Enabled = false;
            }
        }
    }
    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {

    }

    protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Session["userid"] = null;
    }

    // 查询数据库，检查用户是否是管理员
    private bool CheckIfUserIsAdmin(string username)
    {
        bool isAdmin = false;

        string Connstr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        using (SqlConnection connection = new SqlConnection(Connstr))
        {
            connection.Open();
            using (SqlCommand command = new SqlCommand("SELECT user_limit FROM User_info WHERE user_name = @Username", connection))
            {
                command.Parameters.AddWithValue("@Username", username);
                object result = command.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    // 如果user_limit为0，则用户是管理员
                    isAdmin = Convert.ToInt32(result) == 0;
                }
            }
            connection.Close();
        }
        return isAdmin;
    }
}
