﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserInfo.aspx.cs" Inherits="BookShop.UserInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" DataKeyNames="user_id" CellPadding="4" 
    ForeColor="#333333" GridLines="None">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:BoundField DataField="user_gender" HeaderText="性别" 
                SortExpression="user_gender" />
            <asp:BoundField DataField="user_mail" HeaderText="E-mail" 
                SortExpression="user_mail" />
            <asp:BoundField DataField="user_phone" HeaderText="手机号码" 
                SortExpression="user_phone" />
            <asp:BoundField DataField="user_addr" HeaderText="地址" 
                SortExpression="user_addr" />
            <asp:CommandField ShowEditButton="True" />
        </Columns>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
    SelectCommand="SELECT * FROM [User_info]" 
        DeleteCommand="DELETE FROM [User_info] WHERE [user_id] = @user_id" 
        InsertCommand="INSERT INTO [User_info] ([user_id], [user_name], [user_passwd], [user_gender], [user_mail], [user_phone], [user_addr], [user_image_id], [user_limit]) VALUES (@user_id, @user_name, @user_passwd, @user_gender, @user_mail, @user_phone, @user_addr, @user_image_id, @user_limit)" 
        UpdateCommand="UPDATE [User_info] SET [user_name] = @user_name, [user_passwd] = @user_passwd, [user_gender] = @user_gender, [user_mail] = @user_mail, [user_phone] = @user_phone, [user_addr] = @user_addr, [user_image_id] = @user_image_id, [user_limit] = @user_limit WHERE [user_id] = @user_id">
    <DeleteParameters>
        <asp:Parameter Name="user_id" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="user_name" Type="String" />
        <asp:Parameter Name="user_passwd" Type="String" />
        <asp:Parameter Name="user_gender" Type="String" />
        <asp:Parameter Name="user_mail" Type="String" />
        <asp:Parameter Name="user_phone" Type="String" />
        <asp:Parameter Name="user_addr" Type="String" />
        <asp:Parameter Name="user_image_id" Type="Int32" />
        <asp:Parameter Name="user_limit" Type="Int32" />
        <asp:Parameter Name="user_id" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="user_id" Type="Int32" />
        <asp:Parameter Name="user_name" Type="String" />
        <asp:Parameter Name="user_passwd" Type="String" />
        <asp:Parameter Name="user_gender" Type="String" />
        <asp:Parameter Name="user_mail" Type="String" />
        <asp:Parameter Name="user_phone" Type="String" />
        <asp:Parameter Name="user_addr" Type="String" />
        <asp:Parameter Name="user_image_id" Type="Int32" />
        <asp:Parameter Name="user_limit" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
</asp:Content>
