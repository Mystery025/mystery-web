﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OrderInfo.aspx.cs" Inherits="OrderInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" DataKeyNames="orders_id" CellPadding="4" 
    ForeColor="#333333" GridLines="None">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="orders_id" 
                HeaderText="订单编号" SortExpression="orders_id" ReadOnly="True" />
            <asp:BoundField DataField="orders_user_id" HeaderText="用户ID" 
                SortExpression="orders_user_id" />
            <asp:BoundField DataField="orders_time" HeaderText="订单时间" 
                SortExpression="orders_time" />
            <asp:BoundField DataField="orders_status" HeaderText="订单状态" 
                SortExpression="orders_status" />
            <asp:BoundField DataField="orders_amount" HeaderText="订单金额" 
                SortExpression="orders_amount" />
            <asp:BoundField DataField="orders_shipaddr" HeaderText="收货地址" 
                SortExpression="orders_shipaddr" />
            <asp:BoundField DataField="orders_tele" HeaderText="联系电话" 
                SortExpression="orders_tele" />
            <asp:BoundField DataField="orders_name" HeaderText="联系人" 
                SortExpression="orders_name" />
            <asp:HyperLinkField NavigateUrl="Rate.aspx" Text="评价" />
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
    SelectCommand="SELECT * FROM [Orders_info]">
</asp:SqlDataSource>
</asp:Content>

