﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShoppingCart.aspx.cs" Inherits="ShoppingCart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" DataKeyNames="cdetail_id" 
    BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" 
    CellPadding="3" CellSpacing="2">
        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        <Columns>
            <asp:BoundField DataField="cdetail_book_id" HeaderText="书籍ID" 
                SortExpression="cdetail_book_id" />
            <asp:BoundField DataField="cdetail_book_num" HeaderText="数量" 
                SortExpression="cdetail_book_num" />
            <asp:CommandField ShowEditButton="True" />
            <asp:CommandField ShowDeleteButton="True" />
        </Columns>
        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
    SelectCommand="SELECT * FROM [ShoppingCartDetail_info]" 
        DeleteCommand="DELETE FROM [ShoppingCartDetail_info] WHERE [cdetail_id] = @original_cdetail_id" 
        InsertCommand="INSERT INTO [ShoppingCartDetail_info] ([cdetail_id], [cdetail_user_id], [cdetail_book_id], [cdetail_book_num]) VALUES (@cdetail_id, @cdetail_user_id, @cdetail_book_id, @cdetail_book_num)" 
        OldValuesParameterFormatString="original_{0}" 
        UpdateCommand="UPDATE [ShoppingCartDetail_info] SET [cdetail_user_id] = @cdetail_user_id, [cdetail_book_id] = @cdetail_book_id, [cdetail_book_num] = @cdetail_book_num WHERE [cdetail_id] = @original_cdetail_id">
    <DeleteParameters>
        <asp:Parameter Name="original_cdetail_id" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="cdetail_user_id" Type="Int32" />
        <asp:Parameter Name="cdetail_book_id" Type="Int32" />
        <asp:Parameter Name="cdetail_book_num" Type="Int32" />
        <asp:Parameter Name="original_cdetail_id" Type="Int32" />
    </UpdateParameters>
    <InsertParameters>
        <asp:Parameter Name="cdetail_id" Type="Int32" />
        <asp:Parameter Name="cdetail_user_id" Type="Int32" />
        <asp:Parameter Name="cdetail_book_id" Type="Int32" />
        <asp:Parameter Name="cdetail_book_num" Type="Int32" />
    </InsertParameters>
</asp:SqlDataSource>
    <asp:Label ID="Label1" runat="server" Text="地址"></asp:Label>
    <asp:TextBox ID="Addr" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="AddrRequired" runat="server" 
    ControlToValidate="Addr" ErrorMessage="必须填写“地址”。" 
    ToolTip="必须填写“地址”。" ValidationGroup="SqlDataSource">*</asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="Label2" runat="server" Text="电话"></asp:Label>
    <asp:TextBox ID="Tele" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="TeleRequired" runat="server" 
    ControlToValidate="Tele" ErrorMessage="必须填写“电话”。" 
    ToolTip="必须填写“电话”。" ValidationGroup="SqlDataSource">*</asp:RequiredFieldValidator>
    <br />
    <asp:Label ID="Label3" runat="server" Text="姓名"></asp:Label>
    <asp:TextBox ID="Name" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="NameRequired" runat="server" 
    ControlToValidate="Name" ErrorMessage="必须填写“姓名”。" 
    ToolTip="必须填写“姓名”。" ValidationGroup="SqlDataSource">*</asp:RequiredFieldValidator>
    <br />
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="创建订单" />
</asp:Content>
