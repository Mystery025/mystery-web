﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        
    }
    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        TextBox userNameTextBox = (TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("UserName");
        TextBox passwordTextBox = (TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Password");
        DropDownList genderDropDown = (DropDownList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Gender");
        TextBox emailTextBox = (TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Email");
        TextBox phoneNumTextBox = (TextBox)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("PhoneNum");
        FileUpload ImageUpload = (FileUpload)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("ImageUpload");

        // 获取用户输入的信息
        Random rd = new Random();
        int userId = rd.Next()%1000;
        string userName = userNameTextBox.Text;
        string password = passwordTextBox.Text;
        string gender = genderDropDown.SelectedValue;
        string email = emailTextBox.Text;
        string phoneNum = phoneNumTextBox.Text;
        int imageId = 0;
        int limit = 1;
        string Connstr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        // 将用户信息插入到数据库中
        using (SqlConnection connection = new SqlConnection(Connstr))
        {
            connection.Open();

            using (SqlCommand command = new SqlCommand("INSERT INTO User_Info (user_id, user_name, user_passwd, user_gender, user_mail, user_phone, user_image_id, user_limit) VALUES (@UserId, @UserName, @Password, @Gender, @Email, @PhoneNum, @ImageId, @Limit)", connection))
            {
                command.Parameters.AddWithValue("@UserId", userId);
                command.Parameters.AddWithValue("@UserName", userName);
                command.Parameters.AddWithValue("@Password", password);
                command.Parameters.AddWithValue("@Gender", gender);
                command.Parameters.AddWithValue("@Email", email);
                command.Parameters.AddWithValue("@PhoneNum", phoneNum);
                command.Parameters.AddWithValue("@ImageId", imageId);
                command.Parameters.AddWithValue("@Limit", limit);
               
                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        // 处理头像上传
        using (SqlConnection connection = new SqlConnection(Connstr))
        {
            connection.Open();
            using (SqlCommand command = new SqlCommand("INSERT INTO Image_info (image_id, image_data) VALUES (@ImageId, @ImageData);", connection))
            {
                if (ImageUpload != null)
                {
                    if (ImageUpload.HasFile)
                    {
                        // 用户上传了头像
                        int localTime = rd.Next();  
                        string fileName = localTime.ToString() + "_" + ImageUpload.FileName;
                        string filePath = Server.MapPath("~/Uploads/") + fileName;
                        imageId = rd.Next();
                        ImageUpload.SaveAs(filePath);
                        
                        //更新图片表
                        command.Parameters.AddWithValue("@ImageId", imageId);
                        command.Parameters.AddWithValue("@ImageData", filePath);
                        command.ExecuteNonQuery();

                        connection.Close();
                        
                        
                        // 更新用户表中的头像信息
                        /*using (SqlCommand updateCommand = new SqlCommand("UPDATE User_Info SET user_image_id = @ImageId WHERE user_id = @UserId", connection))
                        {
                            updateCommand.Parameters.AddWithValue("@ImageId", imageId);
                            updateCommand.Parameters.AddWithValue("@UserId", userId);
                            updateCommand.ExecuteNonQuery();
                            connection.Close();
                        }*/
                    }
                }
            }
        }
        using (SqlConnection connection = new SqlConnection(Connstr))
        {
            connection.Open();

            using (SqlCommand updateCommand = new SqlCommand("UPDATE User_Info SET user_image_id = @ImageId WHERE user_id = @UserId", connection))
            {
                // 更新用户表中的头像信息
                updateCommand.Parameters.AddWithValue("@ImageId", imageId);
                updateCommand.Parameters.AddWithValue("@UserId", userId);
                updateCommand.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
}
