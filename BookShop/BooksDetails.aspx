﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BooksDetails.aspx.cs" Inherits="BooksDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="book_id" 
        DataSourceID="SqlDataSource1" BackColor="White" BorderColor="White" 
    BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1">
        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
        <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
        <EditItemTemplate>
            书籍编号:
            <asp:Label ID="book_idLabel1" runat="server" Text='<%# Eval("book_id") %>' />
            <br />
            ISBN:
            <asp:TextBox ID="isbn_idTextBox" runat="server" Text='<%# Bind("isbn_id") %>' />
            <br />
            书籍名称:
            <asp:TextBox ID="book_nameTextBox" runat="server" 
                Text='<%# Bind("book_name") %>' />
            <br />
            书籍类型:
            <asp:TextBox ID="book_classTextBox" runat="server" 
                Text='<%# Bind("book_class") %>' />
            <br />
            作者:
            <asp:TextBox ID="book_authorTextBox" runat="server" 
                Text='<%# Bind("book_author") %>' />
            <br />
            出版社:
            <asp:TextBox ID="book_publisherTextBox" runat="server" 
                Text='<%# Bind("book_publisher") %>' />
            <br />
            价格:
            <asp:TextBox ID="book_priceTextBox" runat="server" 
                Text='<%# Bind("book_price") %>' />
            <br />
            库存数量:
            <asp:TextBox ID="book_inventoryTextBox" runat="server" 
                Text='<%# Bind("book_inventory") %>' />
            <br />
            图片:
            <asp:TextBox ID="book_image_idTextBox" runat="server" 
                Text='<%# Bind("book_image_id") %>' />
            <br />
            其他说明:
            <asp:TextBox ID="book_otherTextBox" runat="server" 
                Text='<%# Bind("book_other") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" 
                CommandName="Update" Text="更新" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="取消" />
        </EditItemTemplate>
        <InsertItemTemplate>
            书籍编号:
            <asp:TextBox ID="book_idTextBox" runat="server" Text='<%# Bind("book_id") %>' />
            <br />
            ISBN:
            <asp:TextBox ID="isbn_idTextBox" runat="server" Text='<%# Bind("isbn_id") %>' />
            <br />
            书籍名称:
            <asp:TextBox ID="book_nameTextBox" runat="server" 
                Text='<%# Bind("book_name") %>' />
            <br />
            书籍类型:
            <asp:TextBox ID="book_classTextBox" runat="server" 
                Text='<%# Bind("book_class") %>' />
            <br />
            作者:
            <asp:TextBox ID="book_authorTextBox" runat="server" 
                Text='<%# Bind("book_author") %>' />
            <br />
            出版社:
            <asp:TextBox ID="book_publisherTextBox" runat="server" 
                Text='<%# Bind("book_publisher") %>' />
            <br />
            价格:
            <asp:TextBox ID="book_priceTextBox" runat="server" 
                Text='<%# Bind("book_price") %>' />
            <br />
            出版社:
            <asp:TextBox ID="book_inventoryTextBox" runat="server" 
                Text='<%# Bind("book_inventory") %>' />
            <br />
            图片:
            <asp:TextBox ID="book_image_idTextBox" runat="server" 
                Text='<%# Bind("book_image_id") %>' />
            <br />
            其他说明:
            <asp:TextBox ID="book_otherTextBox" runat="server" 
                Text='<%# Bind("book_other") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" 
                CommandName="Insert" Text="插入" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" 
                CausesValidation="False" CommandName="Cancel" Text="取消" />
        </InsertItemTemplate>
        <ItemTemplate>
            书籍编号:
            <asp:Label ID="book_idLabel" runat="server" Text='<%# Eval("book_id") %>' />
            <br />
            ISBN:
            <asp:Label ID="isbn_idLabel" runat="server" Text='<%# Bind("isbn_id") %>' />
            <br />
            书籍名称:
            <asp:Label ID="book_nameLabel" runat="server" Text='<%# Bind("book_name") %>' />
            <br />
            书籍类型:
            <asp:Label ID="book_classLabel" runat="server" 
                Text='<%# Bind("book_class") %>' />
            <br />
            作者:
            <asp:Label ID="book_authorLabel" runat="server" 
                Text='<%# Bind("book_author") %>' />
            <br />
            出版社:
            <asp:Label ID="book_publisherLabel" runat="server" 
                Text='<%# Bind("book_publisher") %>' />
            <br />
            价格:
            <asp:Label ID="book_priceLabel" runat="server" 
                Text='<%# Bind("book_price") %>' />
            <br />
            库存数量:
            <asp:Label ID="book_inventoryLabel" runat="server" 
                Text='<%# Bind("book_inventory") %>' />
            <br />
            图片:
            <asp:Label ID="book_image_idLabel" runat="server" 
                Text='<%# Bind("book_image_id") %>' />
            <br />
            其他说明:
            <asp:Label ID="book_otherLabel" runat="server" 
                Text='<%# Bind("book_other") %>' />
            <br />
        </ItemTemplate>
        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
        <EditRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        SelectCommand="SELECT * FROM [Book_info] WHERE ([book_id] = @book_id)">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="123" Name="book_id" 
                QueryStringField="book_id" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="加入购物车" />
</asp:Content>
