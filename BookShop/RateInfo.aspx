﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RateInfo.aspx.cs" Inherits="RateInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
    DataSourceID="SqlDataSource1" DataKeyNames="bookrate_id" CellPadding="4" 
    ForeColor="#333333" GridLines="None">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="bookrate_id" 
                HeaderText="评价编号" SortExpression="bookrate_id" ReadOnly="True" />
            <asp:BoundField DataField="bookrate_user_id" HeaderText="用户ID" 
                SortExpression="bookrate_user_id" />
            <asp:BoundField DataField="bookrate_book_id" HeaderText="书籍ID" 
                SortExpression="bookrate_book_id" />
            <asp:BoundField DataField="bookrate_lv" HeaderText="评价星级" 
                SortExpression="bookrate_lv" />
            <asp:BoundField DataField="bookrate_text" HeaderText="评价内容" 
                SortExpression="bookrate_text" />
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
    SelectCommand="SELECT * FROM [BookRate_info]">
</asp:SqlDataSource>
</asp:Content>