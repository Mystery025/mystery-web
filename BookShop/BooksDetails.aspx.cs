﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BooksDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string book_id;
        book_id = Request.QueryString["book_id"];
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Random rd = new Random();
        int cdetail_id = rd.Next();
        int cdetail_user_id;
        string UserName = User.Identity.Name;
        string Connstr2 = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        using (SqlConnection connection2 = new SqlConnection(Connstr2))
        {
            connection2.Open();
            using (SqlCommand command = new SqlCommand("SELECT user_id FROM User_info WHERE user_name = @Username", connection2))
            {
                command.Parameters.AddWithValue("@Username", UserName);
                object result = command.ExecuteScalar();
                if (result != null && result != DBNull.Value)
                {
                    cdetail_user_id = int.Parse(result.ToString());
                    int cdetail_book_id = int.Parse(Request.QueryString["book_id"]);
                    int cdetail_book_num = 1;
                    string Connstr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                    string Sql = "INSERT INTO ShoppingCartDetail_info (cdetail_id,cdetail_user_id,cdetail_book_id,cdetail_book_num) VALUES (@cdetail_id, @cdetail_user_id,@cdetail_book_id,@cdetail_book_num);";
                    using (SqlConnection conn = new SqlConnection(Connstr))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(Sql, conn);
                        cmd.Parameters.Add(new SqlParameter("@cdetail_id", SqlDbType.Int));//设置参数名，类型，长度
                        cmd.Parameters.Add(new SqlParameter("@cdetail_user_id", SqlDbType.Int));//设置参数名，类型，长度
                        cmd.Parameters.Add(new SqlParameter("@cdetail_book_id", SqlDbType.Int));//设置参数名，类型，长度
                        cmd.Parameters.Add(new SqlParameter("@cdetail_book_num", SqlDbType.Int));//设置参数名，类型，长度
                        cmd.Parameters["@cdetail_id"].Value = cdetail_id;//设置参数值
                        // cmd.Parameters["@orders_user_id"].Value = Membership.GetUser().ProviderUserKey.ToString();//设置参数值
                        cmd.Parameters["@cdetail_user_id"].Value = cdetail_user_id;//设置参数值
                        cmd.Parameters["@cdetail_book_id"].Value = cdetail_book_id;//设置参数值
                        cmd.Parameters["@cdetail_book_num"].Value = cdetail_book_num;//设置参数值
                        //Response.Write(cmd.Parameters.ToString());
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                    ClientScript.RegisterStartupScript(GetType(), "message", "添加成功！");
                }
            }
            connection2.Close();
        }
    }
}
