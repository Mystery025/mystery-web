﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ShoppingCart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Random rd = new Random();
        int order_id = rd.Next();
        string Connstr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        string Sql = "INSERT INTO Orders_info (orders_id,orders_user_id,orders_time,orders_status,orders_amount,orders_shipaddr,orders_tele,orders_name) VALUES (@orders_id, @orders_user_id,@orders_time,@orders_status,@orders_amount,@orders_shipaddr,@orders_tele,@orders_name);";
        using (SqlConnection conn = new SqlConnection(Connstr))
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand(Sql, conn);
            cmd.Parameters.Add(new SqlParameter("@orders_id", SqlDbType.Int));//设置参数名，类型，长度
            cmd.Parameters.Add(new SqlParameter("@orders_user_id", SqlDbType.Int));//设置参数名，类型，长度
            cmd.Parameters.Add(new SqlParameter("@orders_time", SqlDbType.Date));//设置参数名，类型，长度
            cmd.Parameters.Add(new SqlParameter("@orders_status", SqlDbType.Int));//设置参数名，类型，长度
            cmd.Parameters.Add(new SqlParameter("@orders_amount", SqlDbType.Float));//设置参数名，类型，长度
            cmd.Parameters.Add(new SqlParameter("@orders_shipaddr", SqlDbType.NVarChar, 100));//设置参数名，类型，长度
            cmd.Parameters.Add(new SqlParameter("@orders_tele", SqlDbType.NVarChar, 11));//设置参数名，类型，长度
            cmd.Parameters.Add(new SqlParameter("@orders_name", SqlDbType.NVarChar, 50));//设置参数名，类型，长度
            cmd.Parameters["@orders_id"].Value = order_id;//设置参数值
            string UserName = User.Identity.Name;
            string UserId;
            string Connstr2 = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection connection2 = new SqlConnection(Connstr2))
            {
                connection2.Open();
                using (SqlCommand command = new SqlCommand("SELECT user_id FROM User_info WHERE user_name = @Username", connection2))
                {
                    command.Parameters.AddWithValue("@Username", UserName);
                    object result = command.ExecuteScalar();
                    if (result != null && result != DBNull.Value)
                    {
                        UserId = result.ToString();
                        cmd.Parameters["@orders_user_id"].Value = UserId;//设置参数值
                        cmd.Parameters["@orders_time"].Value = DateTime.Today;//设置参数值
                        cmd.Parameters["@orders_status"].Value = 2;//设置参数值
                        cmd.Parameters["@orders_amount"].Value = 1;//设置参数值
                        cmd.Parameters["@orders_shipaddr"].Value = Addr.Text;//设置参数值
                        cmd.Parameters["@orders_tele"].Value = Tele.Text;//设置参数值
                        cmd.Parameters["@orders_name"].Value = Name.Text;//设置参数值
                        //Response.Write(cmd.Parameters.ToString());
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
                connection2.Close();
            }
        }
        Response.Redirect("BuySccess.aspx");   
    }
}
