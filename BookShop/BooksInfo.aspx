﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BooksInfo.aspx.cs" Inherits="BooksInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <asp:Button ID="Button1" runat="server" Text="搜索" />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
    AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" 
    OnSelectedIndexChanged="GridView1_SelectedIndexChanged" 
    DataKeyNames="book_id" BackColor="#DEBA84" BorderColor="#DEBA84" 
    BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2">
        <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
        <Columns>
            <asp:BoundField DataField="book_id" HeaderText="书籍编号" ReadOnly="True" 
                SortExpression="book_id" />
            <asp:BoundField DataField="isbn_id" HeaderText="ISBN" 
                SortExpression="isbn_id" />
            <asp:BoundField DataField="book_name" HeaderText="书籍名称" 
                SortExpression="book_name" />
            <asp:BoundField DataField="book_class" HeaderText="书籍类型" 
                SortExpression="book_class" />
            <asp:BoundField DataField="book_author" HeaderText="作者" 
                SortExpression="book_author" />
            <asp:BoundField DataField="book_publisher" HeaderText="出版社" 
                SortExpression="book_publisher" />
            <asp:BoundField DataField="book_price" HeaderText="价格" 
                SortExpression="book_price" />
            <asp:BoundField DataField="book_inventory" HeaderText="库存数量" 
                SortExpression="book_inventory" />
            <asp:BoundField DataField="book_image_id" HeaderText="图片ID" 
                SortExpression="book_image_id" />
            <asp:BoundField DataField="book_other" HeaderText="其他说明" 
                SortExpression="book_other" />
            <asp:CommandField HeaderText="" ShowSelectButton="true" ButtonType="Button" 
                SelectText="详情"/>
        </Columns>
        <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
    SelectCommand="SELECT * FROM [Book_info]">
    </asp:SqlDataSource>
</asp:Content>

